class VehicleBrand < ApplicationRecord
    mount_uploader :image, ImageUploader

    def self.to_csv(options = {})
        CSV.generate(options) do |csv|
            csv << ["Nama", "Tanggal dibuat"]
            all.each do |vehicle_brand|
                csv << [ vehicle_brand.name, vehicle_brand.created_at.strftime("%d-%B-%Y") ]
            end
        end
    end

end
