class VehicleBrandPolicy < ApplicationPolicy

  def index?
    @user.has_role? :admin
  end
  
  def new
    
  end 

  def edit?
    @user.has_role? :admin
  end 

  def show
    
  end 

  class Scope < Scope
    def resolve
      if user.has_role?(:Admin) || user.has_role?(:user)
        scope.all
      else
        scope.all
      end
    end
  end

  private

  def user
    record
  end
end
