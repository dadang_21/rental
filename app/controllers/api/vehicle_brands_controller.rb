class Api::VehicleBrandsController < Api::ApplicationController
  before_action :set_vehicle_brand, only: %i[ show edit update destroy capitalize_font]

  # skip_before_action :verify_authenticity_token
  after_action :capitalize_font, only: [:create]

  # GET /vehicle_brands or /vehicle_brands.json
  def index
    @search = VehicleBrand.ransack(params[:q])
    @vehicle_ransack = @search.result(distinct: true)
    @vehicle_brands = @vehicle_ransack.page(params[:page]).per(5).order(name: :asc)
    render json:{result: @vehicle_brands, status:'success', message: 'data berhasil dibaca'}
    # authorize @vehicle_brands
  end

  def print_index
    if params[:q]
      @search = VehicleBrand.ransack(params[:q])
      @vehicle_brands = @search.result(distinct: true)
    else  
      @vehicle_brands = VehicleBrand.all
    end
    # render json: @vehicle_brands
    respond_to do |format|
      format.csv {send_data @vehicle_brands.to_csv, :filename => 'data_vehicle.csv', :disposition => 'inline', :type => "multipart/related"}
    end  
  end    

  # GET /vehicle_brands/1 or /vehicle_brands/1.json
  def show
  end

  # GET /vehicle_brands/new
  def new
    @vehicle_brand = VehicleBrand.new
  end

  # GET /vehicle_brands/1/edit
  def edit
    authorize @vehicle_brand
  end

  # POST /vehicle_brands or /vehicle_brands.json
  def create
    @vehicle_brand = VehicleBrand.new(vehicle_brand_params)

      if @vehicle_brand.save
          render json: {:status => 'success', message: 'Data Berhasil diinput', result: @vehicle_brand}
      else
          render json: {:status => 'failed', message: 'Data tidak Berhasil diinput'}
      end
  end

  # PATCH/PUT /vehicle_brands/1 or /vehicle_brands/1.json
  def update
      if @vehicle_brand.update(vehicle_brand_params)
        render json: {:status => 'success', message: 'Data Berhasil updated', result: @vehicle_brand}
      else
        render json: {:status => 'failed', message: 'Data tidak Berhasil updated'}
      end
  end

  # DELETE /vehicle_brands/1 or /vehicle_brands/1.json
  def destroy
    @vehicle_brand.destroy

    render json: {:status => 'success', message: 'Data Berhasil dihapus'}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle_brand
      @vehicle_brand = VehicleBrand.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def vehicle_brand_params
      params.require(:vehicle_brand).permit(:name, :image)
    end

    def capitalize_font
      @vehicle_brand_update = @vehicle_brand.update_attributes(:name => @vehicle_brand.name.upcase)
    end
end
