json.extract! vehicle_type, :id, :name, :vehicle_brand_id, :created_at, :updated_at
json.url vehicle_type_url(vehicle_type, format: :json)
