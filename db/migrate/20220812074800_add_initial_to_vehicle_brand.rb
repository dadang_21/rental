class AddInitialToVehicleBrand < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_brands, :creator_id, :integer
    add_column :vehicle_brands, :updater_id, :integer
  end
end
