class AddInitialToVehicleType < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_types, :creator_id, :integer
    add_column :vehicle_types, :updater_id, :integer
  end
end
