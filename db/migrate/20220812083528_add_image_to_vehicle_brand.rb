class AddImageToVehicleBrand < ActiveRecord::Migration[5.2]
  def change
    add_column :vehicle_brands, :image, :string
  end
end
