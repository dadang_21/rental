class CreateVehicleTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicle_types do |t|
      t.string :name
      t.references :vehicle_brand, foreign_key: true

      t.timestamps
    end
  end
end
