Rails.application.routes.draw do
  resources :vehicle_types
  resources :vehicle_brands
  get "print_index" => "vehicle_brands#print_index", as: "print_index"
  get 'pages/home'
  root to:'pages#home'
  devise_for :users, controllers: {
        registrations: 'users/registrations',
        sessions: 'users/sessions'
      }
  resources :users
  resources :roles
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defaults: {format: :json} do
    resources :vehicle_brands
  end
end
